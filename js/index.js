var index = {
	indexPath : "/index",
	getUsuarioDaSessao : function() {
		if (temSessao()) {
		    // Code for localStorage/sessionStorage.
		    return localStorage.getItem("login");
		} else {
		    // Sorry! No Web Storage support..
		    return null;
		}
	},
	setLinkAreaLogada : function() {
		var usuario = index.getUsuarioDaSessao();
		$.ajax({
			url : url + index.indexPath + "/linkAreaLogada",
			async : true,
			data : {
				"usuario" : usuario
			}
		})
		.done(function(retorno) {
			$(".link-index-area-logada").attr('href', urlSite + retorno);
		})
		.fail(function(retorno) {
			console.log(retorno.statusText);
		});
	}
	
};

index.setLinkAreaLogada();